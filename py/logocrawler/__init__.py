import csv
import requests
from bs4 import  BeautifulSoup

urls = []

with open("websites.csv", newline="") as file:
        csv_file = csv.reader(file)
        
        for row in csv_file:
            url = row[0]
            urls.append(url)

logo_urls = []


with open("logo_urls.csv", "r+", newline="") as csv_file:
    field_names = ["website", "logo_url"]
    writer = csv.DictWriter(csv_file, fieldnames=field_names)
    writer.writeheader()

    for url in urls:
        try:
            # upon testing, there are certain websites,
            # which take way too long to load... broken?
            page = requests.get(f"http://{url}", timeout=3)
        except:
            continue

        soup = BeautifulSoup(page.text, "html.parser")

        # since logos are typically the first image on a page,``
        # finding the first image should do.
        # Furthermore, most logo elements don't have "logo" in their
        # class or id names, so "img" will have to do as a query.
        try:
            logo_url = soup.find("img")["src"]

            # only print out logo urls you can access 
            # and filter out non-logo urls
            if not logo_url.startswith("http") or "logo" not in logo_url:
                continue
        except:
            continue

        print({"website": url, "logo_url": logo_url})
        writer.writerow({"website": url, "logo_url": logo_url})




    

    
    
