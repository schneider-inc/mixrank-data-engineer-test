import csv
import requests
from bs4 import  BeautifulSoup
import multiprocessing as mp

urls = []

with open("websites.csv", newline="") as file:
        csv_file = csv.reader(file)
        
        for row in csv_file:
            url = row[0]
            urls.append(url)



def worker(n, q):
    logo_urls = [] 
    for url in urls[int(((n-1)/6)*len(urls)):int((n/6)*len(urls))]:
        try:
            # upon testing, there are certain websites,
            # which take way too long to load... broken?
            page = requests.get(f"http://{url}", timeout=3)
        except:
            continue

        soup = BeautifulSoup(page.text, "html.parser")

        # since logos are typically the first image on a page,``
        # finding the first image should do.
        # Furthermore, most logo elements don't have "logo" in their
        # class or id names, so "img" will have to do as a query.
        try:
            logo_url = soup.find("img")["src"]

            # only print out logo urls you can access 
            # and filter out non-logo urls

            if logo_url.startswith("/") and not logo_url.startswith("//") and "logo" in logo_url:
                logo_url = f"http://{url}{logo_url}"
            elif not logo_url.startswith("http") or "logo" not in logo_url:
                continue
        except:
            continue
        
        print({"website": url, "logo_url": logo_url})
        logo_urls.append({"website": url, "logo_url": logo_url})
        print(n, len(logo_urls))

    q.put(logo_urls)
    return logo_urls

def listener(q):
    with open("logo_urls.csv", "r+", newline="") as csv_file:
        field_names = ["website", "logo_url"]
        writer = csv.DictWriter(csv_file, fieldnames=field_names)
        writer.writeheader()

        logo_urls = []
        while True:
            if q.get() != "kill":
                logo_urls += q.get()
            else:
                break
        for logo_url in logo_urls:
            writer.writerow(logo_url)

if __name__ == "__main__":
    manager = mp.Manager()
    q = manager.Queue()
    pool = mp.Pool(6)

    # listener
    watcher = pool.apply_async(listener, (q,))

    # workers
    jobs = []
    for i in range(1, 7):
        job = pool.apply_async(worker, (i, q))
        jobs.append(job)

    # collect results through pool queue
    for job in jobs:
        job.get()

    q.put("kill")
    pool.close()
    pool.join()

    


    

    
    
